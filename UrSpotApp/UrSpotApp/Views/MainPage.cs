﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;

namespace UrSpotApp.Views
{
    public class MainPage : ContentPage
    {
        public MainPage()
        {
            Title = "Main Page";
       
            Button gotoProfile = new Button
            {
                Text = "Profile",
                HorizontalOptions = LayoutOptions.Center,
                VerticalOptions = LayoutOptions.CenterAndExpand
            };

            gotoProfile.Clicked += async (sender, args) => 
            {
                await Navigation.PushModalAsync(new ProfilePage());
            };

            Button gotoBugs = new Button
            {
                Text = "Bugs",
                HorizontalOptions = LayoutOptions.Center,
                VerticalOptions = LayoutOptions.CenterAndExpand
            };

            gotoBugs.Clicked += async (sender, args) =>
            {
                await Navigation.PushAsync(new BugsPage());
            };

            Button gotoAccount = new Button
            {
                Text = "Account",
                HorizontalOptions = LayoutOptions.Center,
                VerticalOptions = LayoutOptions.CenterAndExpand
            };

            gotoAccount.Clicked += async (sender, args) =>
            {
                await Navigation.PushModalAsync(new AccountPage());
            };

            Content = new StackLayout
            {
                Children =
                {
                    gotoProfile,
                    gotoBugs,
                    gotoAccount
                }
            };
        }
    }
}